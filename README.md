# voxl-meta-package

Repository containing setup scripts used for common vehicle configurations.

These scripts are meant to run on your host PC and communicates to VOXL/VOXL Flight over USB through adb.

## lte

This meta-package is used to configure VOXL to configure the LTE modem on bootup. Run this script from the lte directory of this repository on your host PC with the VOXL attached over USB.

[v1 Modem](https://docs.modalai.com/lte-modem-and-usb-add-on-datasheet/) Requirements:

- System Image: 2.2+
- Factory Bundle: 0.0.4+
- Software Bundle: 0.0.4+

[v2 Modem](https://docs.modalai.com/lte-modem-and-usb-add-on-v2-datasheet/) Requirements:

- System Image: 2.3+
- Factory Bundle: 1.0.1+
- Voxl Suite

Usage:

- Enable LTE radio on VOXL startup

```bash
cd lte

./setup.sh
```

## microhard

This meta-package is used to configure VOXL to configure the Microhard modem on bootup and additionally start an rtsp video stream.  Run this script from the microhard directory of this repository on your host PC with the VOXL attached over USB.

Requirements:

- System Image: 2.2+
- Factory Bundle: 0.0.4+
- Software Bundle: 0.0.4+

Usage:

- Enable Microhard radio on VOXL startup with a static IP address specified by setup script, for example `192.168.168.100`

```bash
cd microhard

./setup.sh 192.168.168.100
```

Note: The chosen IP address must be of the form: `192.168.168.XXX`, where XXX is a 3 digit number

- As above, but and enable `voxl-rtsp` streaming from image sensor instance 0

```bash
./setup.sh 192.168.168.100 -r
```
