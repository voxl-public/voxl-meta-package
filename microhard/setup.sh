#!/bin/bash

################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

# Include helper scripts
source ../common/cli
source ../common/packages
source ../common/system

META_PACKAGE_NAME="microhard"
VERSION=0.0
PREREQ_SYS_IMAGE=2.2.0
PREREQ_FACTORY_BUNDLE=0.0.4
PREREQ_SW_BUNDLE=0.0.4
rtsp=fals

usage(){
	echo "Usage: ./setup voxl_ip [-r]"
}

compare_versions(){
	if [ "$3" == "Software Bundle" ]; then
		REQUIRED_SW_BUNDLE=0.0.6
		NEWEST_SW_VERSION=$(printf $REQUIRED_SW_BUNDLE'\n'$2'\n' | sort -V -r | head -n 1)
		if [ $NEWEST_SW_VERSION != $2 ]; then
			# Check for voxl-modem IPK
			VOXL_MODEM_VER=$(adb shell opkg info voxl-modem | grep Version | awk '{print $2}')
			if [ -z "$VOXL_MODEM_VER" ]; then
				echo -e "\nvoxl-modem IPK not found."
				echo "Since installed Software Bundle is older than 0.0.6, voxl-modem IPK v0.7.6 or higher is required."
				exit
			fi
			# Check version of voxl-modem IPK
			NEWEST_VOXL_MODEM_VER=$(printf $VOXL_MODEM_VER'\n0.7.6\n' | sort -V -r | head -n 1)
			if [ $NEWEST_VOXL_MODEM_VER != $VOXL_MODEM_VER ]; then
				echo -e "\nvoxl-modem IPK is out of date."
				echo "voxl-modem of at least v0.7.6 required."
				echo "Currently installed version: "$VOXL_MODEM_VER
				exit
			fi
		fi
	else
		NEWEST_VER=$(printf $1'\n'$2'\n' | sort -V -r | head -n 1)
		if [ $NEWEST_VER != $2 ]; then
			echo -e "\n"$3" is out of date"
			echo "Please upgrade VOXL "$3" to required version."
			exit
		fi
	fi	
}

prereqs(){
	echo "Required:"
	echo " System Image:    v"$PREREQ_SYS_IMAGE
	echo " Factory Bundle:  v"$PREREQ_FACTORY_BUNDLE
	echo " Software Bundle: v"$PREREQ_SW_BUNDLE
	echo ""
	echo "VOXL:"

	VOXL_VERSION_OUTPUT='adb shell voxl-version'
	VOXL_SYS_IMAGE=$($VOXL_VERSION_OUTPUT' | grep system-image')
	VOXL_SYS_IMAGE=$(echo $VOXL_SYS_IMAGE | awk '{print $3}')
	echo " System Image:    v"$VOXL_SYS_IMAGE

	VOXL_FACTORY_BUNDLE=$($VOXL_VERSION_OUTPUT' | grep factory-bundle')
	VOXL_FACTORY_BUNDLE=$(echo $VOXL_FACTORY_BUNDLE | awk '{print $2}')
	echo " Factory Bundle:  v"$VOXL_FACTORY_BUNDLE
	
	VOXL_SW_BUNDLE=$($VOXL_VERSION_OUTPUT' | grep sw-bundle')
	VOXL_SW_BUNDLE=$(echo $VOXL_SW_BUNDLE | awk '{print $2}')
	echo " Software Bundle: v"$VOXL_SW_BUNDLE
	
	compare_versions $PREREQ_SYS_IMAGE $VOXL_SYS_IMAGE "System Image"
	compare_versions $PREREQ_FACTORY_BUNDLE $VOXL_FACTORY_BUNDLE "Factory Bundle"
	compare_versions $PREREQ_SW_BUNDLE $VOXL_SW_BUNDLE "Software Bundle"
}
if [ $# -eq 0 ]; then
	usage
	exit
fi

# Print Header
common.cli.header $META_PACKAGE_NAME $VERSION

prereqs

if [ $# -eq 1 ]; then
	echo -e "\nVOXL IP: "$1
	sed -i s/192.168.168.100/$1/g microhard-init.service
fi
if [ $# -eq 2 ]; then
	echo -e "\nVOXL IP: "$1
	sed -i s/192.168.168.100/$1/g microhard-init.service
	if [ $2 == "-r" ]; then
		rtsp=true
	fi
fi

echo -e "\nPushing microhard service file to target"
adb push microhard-init.service /etc/systemd/system

if [ "$rtsp" = true ]; then
	echo -e "\nPushing rtsp service file to target"
	adb push voxl-rtsp.service /etc/systemd/system
fi

# Ask whether to enable services on startup
if common.system.enable-startup "Microhard" microhard-init.service; then
	if [ "$rtsp" = true ]; then
		common.system.enable-startup "RTSP" voxl-rtsp.service
	fi
fi

# Reset IP in microhard-init.service file
sed -i s/$1/192.168.168.100/g microhard-init.service

# Make sure all files are saved into the filesystem
adb shell sync

echo -e "\nFinished microhard setup."

# Reboot voxl
echo -e "\nRebooting device"
adb reboot