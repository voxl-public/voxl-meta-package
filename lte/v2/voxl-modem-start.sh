#!/bin/bash

enable-sierra.sh

# Loop until /dev/ttyUSB2 exists
rc=1

while [ $rc -ne 0 ]; do
    ls /dev | grep ttyUSB2
    rc=$?
    sleep 1
done

echo "AT+CGDCONT=1,\"IP\",\"APN\",\"0.0.0.0\",0,0,0,0" > /dev/ttyUSB2

sierra-prepare.sh

# Loop forever trying to get the cellular link up.
# Nothing else matters if this doesn't come up.
rc=1

while [ $rc -ne 0 ]; do
    sierra.py
    rc=$?
    if [[ $rc -eq 0 ]]; then
        exit $rc
    fi
    sleep 1
done
