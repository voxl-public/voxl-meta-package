#!/bin/bash

voxl-modem --feather_configure

echo "AT+CGDCONT=1,\"IP\",\"APN\",\"0.0.0.0\",0,0,0,0" > /dev/ttyACM0

uqmi -P -d /dev/qcqmi0 --set-network-preference lte

configband write /dev/ttyUSB0 0x80a

rc=1

# Loop forever trying to get the cellular link up.
# Nothing else matters if this doesn't come up.
while [ $rc -ne 0 ]; do
    voxl-modem --connect_to_network
    rc=$?
    if [[ $rc -eq 0 ]]; then
        exit $rc
    fi
    sleep 1
done
