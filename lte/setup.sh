#!/bin/bash

################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

# Include helper scripts
source ../common/cli
source ../common/packages
source ../common/system

META_PACKAGE_NAME="lte"
VERSION=0.0
PREREQ_SYS_IMAGE=2.2.0
PREREQ_FACTORY_BUNDLE=0.0.4
PREREQ_SW_BUNDLE=0.0.4
MODEM=''
WORLD=0

usage(){
	echo "Usage: ./setup"
}

# Choose between either v1 or v2 modem
choose_modem(){
	echo -e "\nWhich LTE modem are you using?"
	echo -e "\n[1] v1"
	echo "[2] v2"
	read -p ":" -n 1 -r
	echo -e "\n"

	if [[ $REPLY =~ ^[1]$ ]]
	then
    	MODEM='v1'
	elif [[ $REPLY =~ ^[2]$ ]]
	then
		MODEM='v2'
		PREREQ_SYS_IMAGE=2.3.0
		PREREQ_FACTORY_BUNDLE=1.0.1
	else
		clear
    	echo -e "\nInvalid input, please try again."
		choose_modem
	fi

	echo -e "\nAre you using a North America or World Modem?"
	echo -e "\n[1] North America"
	echo "[2] World"
	read -p ":" -n 1 -r
	echo -e "\n"

	if [[ $REPLY =~ ^[2]$ ]]
	then
		echo -e "\nWorld Modem selected."
		WORLD=1
    	# Comment out configband call
		sed -i s/'configband'/'#configband'/g $MODEM/voxl-modem-start.sh
		
	fi

	
}

# Set APN depending on SIM card service type
choose_carrier(){
	echo -e "\n\nWhich type of SIM card are you using?"
	echo -e "\n[1] AT&T - IoT device - APN: m2m.com.attz"
	echo "[2] AT&T - Laptop or Tablet - APN: broadband"
	echo "[3] AT&T - Smartphone - APN: phone"
	echo "[4] T-Mobile - APN: fast.t-mobile.com"
	echo "[5] Other "
	read -p  ":" -n 1 -r

	if [[ $REPLY =~ ^[1]$ ]]
	then
    	APN='m2m.com.attz'
	elif [[ $REPLY =~ ^[2]$ ]]
	then
		APN='broadband'
	elif [[ $REPLY =~ ^[3]$ ]]
	then
		APN='phone'
	elif [[ $REPLY =~ ^[4]$ ]]
	then
		APN='fast.t-mobile.com'
	elif [[ $REPLY =~ ^[5]$ ]]
	then
		echo -e "\n"
		read -p "Please enter network APN:" APN
	else
		clear
    	echo -e "\nInvalid input, please try again."
		choose_carrier
	fi

	# Modify APN in startup script
	sed -i s/APN/$APN/g $MODEM/voxl-modem-start.sh
}

# Print Header
common.cli.header $META_PACKAGE_NAME $VERSION

choose_modem

echo -e "\nWaiting for device..."
adb wait-for-device
echo -e "Device found.\n"

# Run prereq check
if common.packages.prereqs $PREREQ_SYS_IMAGE $PREREQ_FACTORY_BUNDLE $PREREQ_SW_BUNDLE; then
	echo -e "\nOne or more prerequisites not met."
	exit
else
	echo -e "\nSoftware prerequisites satisfied."
fi

choose_carrier

echo -e "\nPushing lte service file to target"
adb push $MODEM/cellular-start.service /etc/systemd/system

echo -e "\nPushing voxl-modem-start to target"
adb shell touch /usr/bin/voxl-modem-start.sh
adb push $MODEM/voxl-modem-start.sh /usr/bin/.
adb shell chmod +x /usr/bin/voxl-modem-start.sh

echo -e "\nPushing VPN files to target"
adb push vpn/time-start.service /etc/systemd/system
adb push vpn/timecheck.py /usr/local/bin
adb push vpn/vpn-start.service /etc/systemd/system

# Ask whether to enable services on startup
echo 
read -p "Would you like to have the LTE service start on bootup? (y/n): " -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo -e "\nEnabling LTE service..."
    adb shell sudo systemctl enable cellular-start.service
	adb shell sudo systemctl enable time-start.service
fi

# Revert APN in startup script
sed -i s/$APN/APN/g $MODEM/voxl-modem-start.sh

if [ $WORLD -eq 1 ]; then
	# Revert configband comment
	sed -i s/'#configband'/'configband'/g $MODEM/voxl-modem-start.sh
fi

# Make sure all files are saved into the filesystem
adb shell sync

echo -e "\nFinished LTE setup."

# Reboot voxl
echo -e "\nRebooting device"
adb reboot
